import React, { Component, Fragment } from 'react'
import Slide from './Slide';
import isEmpty from 'lodash/isEmpty'
import isEqual from 'lodash/isEqual'
import get from 'lodash/get'
import {
    Slider,
    SliderInner,
    SliderScrollWrap,
    SliderPrevArrow,
    SliderNextArrow,
    SliderDots,
    SliderDot,
} from './styles'


export default class Carousel extends Component {
    constructor(props) {
        super(props)
        this.id = props.id

        let { slidesToShow,
            slidesToScroll,
            arrows,
            dots,
            preloader,
            slides,
            height,
            vertical,
            free,
            arrowPrev,
            arrowNext,
            breakpoints,
            selectedMode,
            focusedSlideIndex,
            autoplay,
            autoplaySpeed,
            speed,
            infinite, //TODO: fix the need of an height for infinite slider
            index,
            nativeMobile,
            // TODO: add native scroll on desktop
        } = this.props

        // Used for touch events
        this.touch = {}

        this.containerNode = React.createRef()
        this.innerContainerNode = React.createRef()

        this.scrollTimeout
        this.scrollTimeStamp

        if (slides.length == 2 && infinite) {
            slides = slides.concat(slides)
        }
        this.state = {
            currentSlideIndex: index || 0,
            focusedSlideIndex: focusedSlideIndex || 0,
            lastSlideIndex: null,
            currenSlide: null, // might be useless
            slides: slides || [],
            free: free || false,

            // dimensions
            windowWidth: 0,
            containerWidth: 0,
            slideWidth: 0,

            style: {
                // overflow: 'hidden',
                // overflowX: 'scroll', //mobile only 
            },
            slideHeight: height || null,

            // settings
            slidesToShow: slidesToShow || 1,
            slidesToScroll: slidesToScroll || 1,
            arrows: (typeof arrows !== 'undefined') ? arrows : true,
            dots: dots || false,
            preloader: preloader || false,
            arrowPrev,
            arrowNext,
            vertical: vertical || false,
            selectedMode: selectedMode || false,

            autoplay: autoplay || false,
            autoplaySpeed: autoplaySpeed || 2000,
            autoplayInterval: null,
            disableAnimation: true,
            infinite: infinite || false,
            infiniteIndex: 1,
            speed: speed || 300,

            isDisabled: false,
            nativeMobile: nativeMobile || false,

            breakpoints: breakpoints || {},
            interaction: false,
            clickedOnBullets: false
        }

        // this.clickedOnBullets = false
    }

    // Handle updates
    componentWillReceiveProps(nextProps) {
        const { selectedMode, slides, currentSlideIndex } = this.state

        let nextIndex = nextProps.index
        nextIndex = Math.max(0, Math.min(nextIndex, slides.length - this.getSetting('slidesToShow')))
        const newState = {}
        if ((nextIndex || nextIndex == 0) && nextIndex != currentSlideIndex) {
            if (!selectedMode || (selectedMode && nextIndex >= currentSlideIndex + this.getSetting('slidesToShow'))) {
                newState.currentSlideIndex = nextIndex
            }

        }
        const focusedSlideIndex = Math.max(0, Math.min(nextProps.focusedSlideIndex || nextProps.index, nextProps.slides.length - 1))

        if (selectedMode) {
            if (focusedSlideIndex != this.state.focusedSlideIndex) {
                newState.focusedSlideIndex = focusedSlideIndex
                if (this.state.currentSlideIndex < focusedSlideIndex
                    && Math.abs(this.state.currentSlideIndex - focusedSlideIndex) > Math.ceil(this.getSetting('slidesToShow') / 2)) {
                    let index = (focusedSlideIndex - Math.ceil(this.getSetting('slidesToShow') / 2))
                    if (focusedSlideIndex == this.state.slides.length - 1) index = this.state.slides.length - this.getSetting('slidesToShow')
                    newState.currentSlideIndex = index
                } else if (this.state.currentSlideIndex == focusedSlideIndex) {
                    newState.currentSlideIndex = Math.max(0, (this.state.currentSlideIndex - (Math.max(this.getSetting('slidesToScroll') - 2, 1))))
                }
            }
        }

        // TODO: fix slides comparison, lodash isEqual doesnt always works on arrays of react components
        if (!isEqual(this.state.slides, nextProps.slides)) newState.slides = nextProps.slides
        if (!isEmpty(newState)) this.setState(newState)
    }

    setDimensions = () => {
        const containerNode = this.containerNode.current
        const containerWidth = containerNode.getBoundingClientRect().width
        const containerHeight = containerNode.getBoundingClientRect().height

        this.setState({
            containerWidth,
            containerHeight,
            windowWidth: window.innerWidth,
        })
    }

    componentDidMount() {
        this.setDimensions()
        window.addEventListener('resize', this.setDimensions)
        this.containerNode.current.addEventListener('touchmove', this._onTouchMove)
        this.initAutoplay()

        if (this.state.disableAnimation) {
            setTimeout(() => {
                this.setState({
                    disableAnimation: false
                })
            }, 10)

        }
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.setDimensions)
        this.containerNode.current.removeEventListener('touchmove', this._onTouchMove)
        this.disableAutoplay()
    }

    initAutoplay = () => {
        const autoplay = this.getSetting('autoplay')

        if (autoplay && !this.state.interaction) {
            this.startSlide = Date.now()
            const time = this.duration ? this.getSetting('autoplaySpeed') - this.elapsed : this.getSetting('autoplaySpeed')
            if (this.state.autoplayInterval) {
                clearInterval(this.state.autoplayInterval)
            }
            const interval = setInterval(
                () => {
                    this.startSlide = Date.now()
                    this.elapsed = 0
                    if (this.duration) {
                        this.duration = 0
                        this.disableAutoplay()
                        this.initAutoplay()
                    }
                    this.goNext('autoplay')
                },
                time
            )
            this.setState({ autoplayInterval: interval })
        }
    }

    disableAutoplay = () => {
        if (this.state.autoplayInterval) {
            clearInterval(this.state.autoplayInterval)
        }
    }

    // callbacks
    onScroll = () => {
        const { onScroll } = this.props
        if (onScroll) {
            const { currentSlideIndex, focusedSlideIndex, slides } = this.state
            onScroll({ currentSlideIndex, focusedSlideIndex, slides })
        }
    }

    onSlide = () => {
        const { onSlide } = this.props
        if (onSlide) {
            const { currentSlideIndex, focusedSlideIndex, slides } = this.state
            onSlide({ currentSlideIndex, focusedSlideIndex, slides, id: this.id })
        }
    }
    onAfterSlide = () => {
        const { onAfterSlide } = this.props
        if (onAfterSlide) {
            const { currentSlideIndex, focusedSlideIndex, slides } = this.state
            onAfterSlide({ currentSlideIndex, focusedSlideIndex, slides, id: this.id })
        }
    }

    // events
    handleScroll = (e) => {
        const { slides } = this.state
        let currentScroll = this.containerNode.current.scrollLeft
        const scrollWidth = this.containerNode.current.scrollWidth
        const oneSlideScroll = scrollWidth / slides.length
        let scrollSlideIndex = currentScroll / oneSlideScroll
        scrollSlideIndex = Math.round(scrollSlideIndex)

        const timeStamp = e.nativeEvent.timeStamp

        if (this.scrollTimeout) clearTimeout(this.scrollTimeout)

        this.scrollTimeout = setTimeout(() => {
            if (timeStamp == this.scrollTimeStamp) {
                this.setState({
                    currentSlideIndex: scrollSlideIndex,
                    lastSlideIndex: scrollSlideIndex
                })
            }
        }, 75)

        this.scrollTimeStamp = timeStamp
    }

    changeSlideIndex = (newIndex, focusedSlideIndex) => {
        const { selectedMode, isDisabled, speed, clickedOnBullets, slides } = this.state

        if (isDisabled) return
        // if (clickedOnBullets) {
        //     this.setState({ currentSlideIndex: newIndex, infiniteIndex: 0, lastSlideIndex: this.state.currentSlideIndex })
        //     return
        // }
        if (newIndex != this.state.currentSlideIndex || (selectedMode && focusedSlideIndex != this.state.focusedSlideIndex)) {

            this.startSlide = Date.now()
            this.duration = null
            this.elapsed = 0

            if (selectedMode) {
                if (focusedSlideIndex - this.state.currentSlideIndex <= Math.ceil(this.getSetting('slidesToShow') / 2)
                    && focusedSlideIndex > this.state.currentSlideIndex) {
                    newIndex = this.state.currentSlideIndex
                }
            }
            if (newIndex < 0 || newIndex > slides.length - 1) {

                // TODO: clear timeout if the user clicks more than once
                setTimeout(() => {
                    this.setState({
                        disableAnimation: true,
                        currentSlideIndex: slides.length - Math.abs(newIndex),
                    }, () => {
                        setTimeout(() => {
                            this.setState({
                                disableAnimation: false
                            })
                        }, 10)
                    })
                }, speed)
            }
            this.setState({
                currentSlideIndex: newIndex,
                lastSlideIndex: this.state.currentSlideIndex,
                focusedSlideIndex,
                isDisabled: true
            }, () => {
                this.onSlide()

                setTimeout(() => {
                    this.setState({ isDisabled: false })
                    this.onAfterSlide()
                }, speed)
            })
        }
    }

    // actions
    goPrev = () => {
        const { currentSlideIndex, focusedSlideIndex, infinite } = this.state
        const slidesToScroll = this.getSetting('slidesToScroll')

        const prevSlideIndex = Math.max(infinite ? -1 : 0, currentSlideIndex - slidesToScroll)
        const prevFocusedSlideIndex = Math.max(focusedSlideIndex - slidesToScroll, 0)
        this.changeSlideIndex(prevSlideIndex, prevFocusedSlideIndex)

        this.setState({ clickedOnBullets: true, interaction: true, autoplay: false })
        // this.clickedOnBullets = false

        // Disable autoplay
        if (this.getSetting('autoplay')) {
            this.duration = null
            // this.initAutoplay()
            this.disableAutoplay()
            // clearInterval(this.state.autoplayInterval)
            // this.setState({ interaction: true, autoplay: false })
        }
    }

    goNext = (param) => {
        const { currentSlideIndex, slides, focusedSlideIndex, lastSlideIndex, interaction, infinite } = this.state
        const slidesToScroll = this.getSetting('slidesToScroll')
        const slidesToShow = this.getSetting('slidesToShow')
        let nextSlideIndex = Math.min(slides.length - slidesToShow + (infinite ? slidesToScroll : 0), currentSlideIndex + slidesToScroll)
        let nextFocusedSlideIndex = Math.min(slides.length, focusedSlideIndex + slidesToScroll)

        if (typeof param == 'object') this.setState({ clickedOnBullets: true, interaction: true, autoplay: false })

        if (this.getSetting('autoplay')) {
            this.duration = null
            // this.initAutoplay()
            if (typeof param == 'object') this.disableAutoplay()
        }

        this.changeSlideIndex(nextSlideIndex, nextFocusedSlideIndex)
    }

    getSetting(setting) {
        let { windowWidth, breakpoints } = this.state

        // INFO: on SSR always render the biggest breakpoint
        if (typeof window == 'undefined') windowWidth = 9999999
        const currentBreakPoint = Object.keys(breakpoints)
            .map(i => parseInt(i))
            .sort((a, b) => b - a)
            .reduce((result, item, index) => {
                if (windowWidth <= item) result = item
                return result
            }, null)

        return !currentBreakPoint ? this.state[setting] : get(breakpoints, `${currentBreakPoint}.${setting}`, this.state[setting])
    }

    getInnerContainerStyles = () => {
        const { slides, containerWidth, containerHeight,
            speed, windowWidth, vertical, selectedMode, focusedSlideIndex,
            disableAnimation, currentSlideIndex, nativeMobile, infinite, } = this.state

        let slidesLength = slides.length
        let fIndex = currentSlideIndex
        const styles = {
            transitionDuration: disableAnimation ? '0ms' : `${speed}ms`,
            width: vertical ? `100%` : `${slides.length / this.getSetting('slidesToShow') * 100}%`,
        }
        if (vertical) styles.height = `${slidesLength * (containerHeight / this.getSetting('slidesToShow'))}px`

        if (windowWidth > 1024 || !nativeMobile) {
            let transform = `translate3d(${-1 * currentSlideIndex * (1 / slidesLength * 100)}%, 0px, 0px)`

            if (vertical) {
                if (selectedMode && (focusedSlideIndex || focusedSlideIndex == 0) && currentSlideIndex) {
                    fIndex = Math.min(focusedSlideIndex, currentSlideIndex)
                }
                transform = `translate3d(0px, -${Math.min(fIndex, slidesLength - this.getSetting('slidesToShow')) * (1 / slides.length * 100)}%, 0px)`
            }

            styles.transform = transform
        }

        return styles
    }

    getContainerStyles = () => {
        let { styles } = this.state
        const { windowWidth, slides, currentSlideIndex, lastSlideIndex, free, vertical, slideHeight, nativeMobile } = this.state

        if (windowWidth <= 1024 && this.containerNode.current && nativeMobile) {
            let currentScroll = this.containerNode.current.scrollLeft
            const scrollWidth = this.containerNode.current.scrollWidth
            const oneSlideScroll = scrollWidth / slides.length

            currentScroll = free
                ? currentScroll
                : Math.max(0, (currentSlideIndex - 1)) * oneSlideScroll // add the current index scroll amount. Used when changing from desktop to mobile size

            const nextScroll = free
                ? lastSlideIndex != currentSlideIndex ? currentSlideIndex * oneSlideScroll : currentScroll
                : currentSlideIndex * oneSlideScroll

            this.containerNode.current.scrollLeft = nextScroll
        }
        styles = styles || {}
        if (vertical) styles.height = `${slideHeight}px`

        return styles
    }

    onChangeDots(index) {
        this.setState({ clickedOnBullets: true })
        // Disable autoplay
        if (this.getSetting('autoplay')) {
            clearInterval(this.state.autoplayInterval)
            this.setState({ interaction: true, autoplay: false }, () => {
                this.changeSlideIndex(index)
            })
        } else {
            this.changeSlideIndex(index)
        }

    }

    renderDots = () => {
        const { slides, preloader, infiniteIndex, infinite, interaction } = this.state
        let { currentSlideIndex } = this.state
        return (
            <SliderDots
                className={this.createClassname("slider-dots" + (preloader ? ' preloader-list' : ''))}>
                {slides.map((item, index) => {
                    const isActive = currentSlideIndex == index
                        || (currentSlideIndex < 0 && index == slides.length - 1)
                        || (currentSlideIndex >= slides.length && index == 0)
                    return (
                        <SliderDot
                            key={`slider-dot-${index}`}
                            onClick={this.onChangeDots.bind(this, index)}
                            className={this.createClassname(`slider-dot ${isActive ? 'active' : ''}`)}>
                            {preloader && <span className={`in ${interaction ? 'full' : ''}`}></span>}
                        </SliderDot>
                    )
                })}
            </SliderDots>
        )
    }

    renderPrevArrow() {
        const { slides, currentSlideIndex, arrowPrev, infinite } = this.state

        if (!(this.getSetting('arrows') && slides.length > this.getSetting('slidesToShow'))) return null

        const isDisabled = currentSlideIndex == 0 && !infinite

        return React.cloneElement(
            (arrowPrev || <button>{'<'}</button>),
            {
                ...(isDisabled ? { disabled: true } : {}),
                onClick: this.goPrev,
                className: [
                    get(arrowPrev, 'props.className', null),
                    isDisabled ? 'disabled' : null
                ].filter(i => i).join(' ')
            }
        )

    }

    renderNextArrow() {
        const { slides, currentSlideIndex, arrowNext, infinite } = this.state

        if (!(this.getSetting('arrows') && slides.length > this.getSetting('slidesToShow'))) return null

        const isDisabled = slides.length - this.getSetting('slidesToShow') == currentSlideIndex && !infinite

        return React.cloneElement(
            (arrowNext || <button>{'>'}</button>),
            {
                ...(isDisabled ? { disabled: true } : {}),
                onClick: this.goNext,
                className: [
                    get(arrowNext, 'props.className', null),
                    isDisabled ? 'disabled' : null
                ].filter(i => i).join(' ')
            }
        )
    }

    handleMouseOver() {
        const { autoplay } = this.state

        this.elapsed = (this.elapsed || 0) + (Date.now() - this.startSlide)

        this.start = Date.now()
        if (autoplay) {
            this.disableAutoplay()
        }
    }

    handleMouseOut = () => {
        if (this.duration !== null) {
            this.duration = (this.duration || 0) + (Date.now() - this.start)
        } else {
            this.duration = 0
        }
        if (!this.state.interaction) this.initAutoplay()
    }

    shouldSlideChangeOnTouch = touch => {
        const absX = Math.abs(touch.clientX - this.touch.start.x)
        const diffX = Math.abs(this.touch.start.x - touch.clientX)
        const diffY = Math.abs(this.touch.start.y - touch.clientY)
        const angle = Math.atan2(diffX, diffY) * 180 / Math.PI

        return absX > (this.state.touchThreshold || 50) && angle > 60
    }

    // Touch events
    _onTouchStart = (e) => {
        const touch = e.touches[0]
        this.touch.start = { x: touch.clientX, y: touch.clientY }
        const { interaction } = this.state
        const autoplay = this.getSetting('autoplay')
        
        // Disable autoplay
        if (autoplay || !interaction) {
            this.setState({ interaction: true, autoplay: false })
            this.duration = null
            this.disableAutoplay()
        }
    }

    _onTouchEnd = (e) => {
        const { nativeMobile } = this.state
        if (nativeMobile) return

        const touch = e.changedTouches[0]
        const direction = Math.sign(touch.clientX - this.touch.start.x)

        if (this.shouldSlideChangeOnTouch(touch)) { //TODO: make angle a prop
            if (direction > 0 && direction) {
                this.goPrev()
            } else {
                this.goNext()
            }
        }
    }

    _onTouchMove = (e) => {
        const touch = e.changedTouches[0]

        //prevent vertical scrolling -- IOS fix
        if (this.shouldSlideChangeOnTouch(touch)) {
            e.preventDefault()
        }
        // if (e.changedTouches && e.changedTouches.length) {
        //     const touch = e.changedTouches[0]
        // }
    }

    getSlideWidth = () => {
        const { slides, vertical, } = this.state
        return vertical ? '100%' : `${100 / slides.length}%`
    }

    createClassname = (className) => {
        const { classPrefix, classSuffix } = this.props

        return `${classPrefix || ''}${className}${classSuffix || ''}`
    }

    render() {
        const { slideHeight,
            slides,
            slidesToShow,
            currentSlideIndex,
            focusedSlideIndex,
            selectedMode,
            dots,
            height,
            vertical,
            infinite,
            nativeMobile,
        } = this.state
        let { infiniteIndex } = this.state

        let fIndex = (selectedMode ? focusedSlideIndex : 0) || currentSlideIndex
        if ((selectedMode && (focusedSlideIndex || focusedSlideIndex == 0)) && currentSlideIndex) {
            fIndex = Math.min(focusedSlideIndex, currentSlideIndex)
        }

        let slidesToRender = [...slides]

        const containerStyles = this.getContainerStyles()
        const innerContainerStyles = this.getInnerContainerStyles()
        if (innerContainerStyles.width != "0px") {
            innerContainerStyles.overflow = "visible"
        }

        return (
            <Slider
                className={this.createClassname("slider")}
                onMouseEnter={this.handleMouseOver.bind(this)}
                onMouseLeave={this.handleMouseOut.bind(this)}>
                <SliderPrevArrow className={this.createClassname("slider-prev-left")}>
                    {this.renderPrevArrow()}
                </SliderPrevArrow>


                <SliderScrollWrap
                    className={this.createClassname(`slide-scroll-wrap ${!nativeMobile ? 'no-scroll' : ''}`)}
                    ref={this.containerNode}
                    style={containerStyles}
                    onScroll={this.handleScroll}
                    onTouchStart={this._onTouchStart}
                    onTouchEnd={this._onTouchEnd}
                    // onTouchMove={this._onTouchMove}
                >
                    <SliderInner
                        className={this.createClassname("slider-inner")}
                        ref={this.innerContainerNode}
                        style={{ ...innerContainerStyles, height: slideHeight }}>
                        {
                            slidesToRender.map((slide, index) => {
                                let className = []
                                const visible = index >= fIndex && index < fIndex + this.getSetting('slidesToShow')
                                if (visible) className.push('visible')
                                if (index == fIndex) className.push('first-visible')
                                if (index == fIndex + this.getSetting('slidesToShow') - 1 || (index == slidesToRender.length - 1 && visible)) className.push('last-visible')
                                if (selectedMode && (focusedSlideIndex || focusedSlideIndex == 0)) {
                                    if (index == focusedSlideIndex) className.push('active')
                                } else {
                                    if (index == currentSlideIndex) className.push('active')
                                }

                                className = className.join(" ")

                                const isFirst = currentSlideIndex >= slides.length - 1 && index == 0
                                const isLast = currentSlideIndex <= 0 && index == slides.length - 1
                                const infiniteSlideStyle = infinite && innerContainerStyles.width != "0px" ? {
                                    position: 'absolute',
                                    'overflow': 'hidden',
                                    left: `${isFirst ? 100 : (isLast ? -100 / slidesToRender.length : index * 100 / slidesToRender.length)}%`
                                } : {}

                                return (

                                    <Fragment key={`slide-${index}`}>
                                        <Slide
                                            style={infiniteSlideStyle}
                                            {...(className ? { className } : {})}
                                            content={slide}
                                            width={this.getSlideWidth()}
                                            height={vertical ? slideHeight / this.getSetting(slidesToShow) : (slideHeight || null)} />
                                    </Fragment>
                                )
                            })
                        }
                    </SliderInner>
                </SliderScrollWrap>

                {dots &&
                    this.renderDots()
                }
                <SliderNextArrow className={this.createClassname("slider-next-right")}>
                    {this.renderNextArrow()}
                </SliderNextArrow>
            </Slider>
        )
    }
}