import styled from 'styled-components'

export const Slider = styled.div`
    
`

export const SliderInner = styled.div`
    overflow: hidden;
    transition: transform ease-in-out;
    box-sizing: border-box;
    position: relative;

    &:after {
        display: table;
        content: "";
        clear: both;
    }
`

export const SliderDots = styled.ul`
    position: absolute;
    bottom: 0;
    left: 50%;
    transform: translate(-50%);
`

export const SliderDot = styled.li`
    display: inline-block;
    width: 10px;
    height: 10px;
    margin: 5px;
    background: #215196;
    border-radius: 50%;
    cursor: pointer;
`

export const SliderScrollWrap = styled.div`
    overflow: hidden;

    @media screen and (max-width: 1024px) {
        overflow-x: scroll;
        scroll-behavior: smooth;

        &.no-scroll {
            overflow-x: hidden;
        }
    }
`
export const SliderPrevArrow = styled.div`
    z-index: 2;
`

export const SliderNextArrow = styled.div`
    z-index: 2;
`