import React, { Component, Fragment } from 'react'
import get from 'lodash/get'

export default class Slide extends Component {
    constructor(props) {
        super(props)

        this.state = {}
    }

    getStyle() {
        const { width, height, style } = this.props

        return {
            float: 'left',
            boxSizing: 'border-box',
            width,
            ...(height ? { height } : {}),
            ...style
        }
    }

    render() {
        const { className, content, ...rest } = this.props
        let contentClassName = get(this.props, 'content.props.className', '')

        const cls = [contentClassName, className].filter(i => i).join(" ")

        if (!this.props.content) return null
        return (
            <Fragment>
                {
                    React.cloneElement(
                        this.props.content,
                        { style: this.getStyle(), className: cls }
                    )
                }
            </Fragment>
        )
    }
}