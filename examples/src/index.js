import React from 'react'
import ReactDOM from 'react-dom'
import shuffle from 'lodash/shuffle'
import Highlight from 'react-highlight.js'
import Slider from '../../src'
import './examples.css'


const PROPERTIES = [
    {
        name: 'slides',
        type: "array",
        default: '[]',
        description: 'An array of slides to be shown. The items need to be html elements',
    },
    {
        name: 'slidesToShow',
        type: "int (float can also be used but it's not really recommended at the moment)",
        default: 1,
        description: 'Number of slides to show',
    },
    {
        name: 'slidesToScroll',
        type: "int (float can also be used but it's not really recommended at the moment)",
        default: 1,
        description: 'Number of slides to scroll',
    },
    {
        name: 'infinite',
        type: "boolean",
        default: 'false',
        description: 'Toggles infinite loop sliding',
    },
    {
        name: 'speed',
        type: "int",
        default: 300,
        description: 'Slide animation speed in milliseconds',
    },
    {
        name: 'autoplaySpeed',
        type: "int",
        default: 2000,
        description: 'Autoplay speed in milliseconds',
    },
    {
        name: 'dots',
        type: "boolean",
        default: 'false',
        description: 'Toggles the visibility of the slides dots',
    },
    {
        name: 'arrows',
        type: "boolean",
        default: 'true',
        description: 'Toggles the visibility of the navigation arrows',
    },
    {
        name: 'nativeMobile',
        type: "boolean",
        default: 'false',
        description: 'If true and the width of the page is < 1024) the slider uses native scroll',
    },
    {
        name: 'free',
        type: "boolean",
        default: 'false',
        description: 'If true and the width of the page is < 1024) the slider scroll is free',
    },
    {
        name: 'height',
        type: "int",
        default: 'undefined',
        description: 'If set, the slider will have the height set in pixels',
    },
]

class Example extends React.Component {
    render() {
        const slides = [
            <div><img src="https://images.unsplash.com/photo-1522057306606-8d84daa75e87?i&fit=crop&w=800&h=600&q=80" /></div>,
            <div><img src="https://images.unsplash.com/photo-1446292267125-fecb4ecbf1a5?i&fit=crop&w=800&h=600&q=80" /></div>,
            <div><img src="https://images.unsplash.com/photo-1558387886-d6988f49a2e0?i&fit=crop&w=800&h=600&q=80" /></div>,
            <div><img src="https://images.unsplash.com/photo-1558386620-41b4aef9fd5a?i&fit=crop&w=800&h=600&q=80" /></div>,
            <div><img src="https://images.unsplash.com/photo-1558383329-97dd01b09510?i&fit=crop&w=800&h=600&q=80" /></div>,
            <div><img src="https://images.unsplash.com/photo-1569387623950-11c5f5be2f38?i&fit=crop&w=800&h=600&q=80" /></div>,
            <div><img src="https://images.unsplash.com/photo-1525534240745-6b6f65e8a25f?i&fit=crop&w=800&h=600&q=80" /></div>,
            <div><img src="https://images.unsplash.com/photo-1558393383-f6f7f7f9ef1c?i&fit=crop&w=800&h=600&q=80" /></div>,
            <div><img src="https://images.unsplash.com/photo-1532365673558-f9bb768644e7?i&fit=crop&w=800&h=600&q=80" /></div>,
            <div><img src="https://images.unsplash.com/photo-1574676122993-cd7fcf7f9030?i&fit=crop&w=800&h=600&q=80" /></div>,
        ]
        return (
            <div className="container">
                <h1>awesome-react-carousel</h1>
                <div className="example">
                    <h3>Single item</h3>
                    <Slider
                        slides={shuffle(slides)}
                        slidesToShow={1}
                    />
                    <Highlight language="html">
                        {'<Slider\nslides={slides}\nslidesToShow={1}\n/>'}
                    </Highlight>
                </div>
                <div className="example">
                    <h3>Multiple items</h3>
                    <Slider
                        slides={shuffle(slides)}
                        slidesToShow={3}
                        slidesToScroll={3}
                    />
                    <Highlight language="html">
                        {'<Slider\nslides={slides}\nslidesToShow={3}\nslidesToScroll={3}\n/>'}
                    </Highlight>
                </div>

                <div className="example">
                    <h3>Autoplay</h3>
                    <Slider
                        slides={shuffle(slides)}
                        slidesToShow={3}
                        slidesToScroll={1}
                        autoplay={true}
                        autoplaySpeed={1500}
                    />
                    <Highlight language="html">
                        {'<Slider\nslides={slides}\nslidesToShow={3}\nslidesToScroll={1}\nautoplay={true}\nautoplaySpeed={1500}\n/>'}
                    </Highlight>
                </div>

                <div className="example">
                    <h3>Infinite</h3>
                    <Slider
                        slides={shuffle(slides)}
                        infinite={true}
                        height={300}
                        autoplay={true}
                        autoplaySpeed={1000}
                    />
                    <Highlight language="html">
                        {'<Slider\nslides={slides}\ninfinite={true}\nheight={300}\nautoplay={true}\nautoplaySpeed={1000}\n/>'}
                    </Highlight>
                    <p className="note">NOTE: at the moment the infinite slider needs the height property. This will be fixed in a later version.</p>
                </div>

                <div className="example">
                    <h3>Free touch on mobile</h3>
                    <Slider
                        slides={shuffle(slides)}
                        nativeMobile={true}
                        free={true}
                    />
                    <Highlight language="html">
                        {'<Slider\nslides={slides}\nnativeMobile={true}\nfree={true}\n/>'}
                    </Highlight>
                </div>

                <div className="example">
                    <h3>Free touch with fixed slide</h3>
                    <Slider
                        slides={shuffle(slides)}
                        nativeMobile={true}
                    />
                    <Highlight language="html">
                        {'<Slider\nslides={slides}\n/>'}
                    </Highlight>
                </div>
                <div className="example">
                    <h3>Arrows</h3>
                    <Slider
                        slides={shuffle(slides)}
                        arrows={false}
                    />
                    <Highlight language="html">
                        {'<Slider\nslides={slides}\ninfinite={true}\nheight={300}\nautoplay={true}\nautoplaySpeed={1000}\n/>'}
                    </Highlight>
                </div>

                <div className="usage">
                    <h3>Usage</h3>
                    <Highlight language="html">
                        {'import Slider from "awesome-react-slider"\n'}
                        {'const slides = [<p>slides1</p>, <p>slide2</p>, <p>slide3</p>]\n'}
                        {'<Slider\nslides={slides}\nnativeMobile={true}\n/>'}
                    </Highlight>
                </div>

                <div className="example">
                    <h3>Properties</h3>
                    {
                        PROPERTIES.map((item, index) => {
                            return (
                                <div
                                    key={`slider-property-${index}`}
                                    className="property">
                                    <p className="prop-title">{item.name}</p>
                                    <p className="prop"><strong>Type: </strong>{item.type}</p>
                                    <p className="prop"><strong>Default: </strong>{item.default}</p>
                                    <p className="prop">{item.description}</p>
                                </div>
                            )
                        })
                    }
                </div>

            </div>
        )
    }
}

ReactDOM.render(<Example />, document.getElementById('app'))