A carousel/slide component build with React.

## Documentation
#### Installation

    yarn add awesome-react-carousel
    npm install awesome-react-carousel

#### Example
```js
    import Slider from "awesome-react-carousel"
    
	render(){
		const slides = [
			<div>slide1</div>,
			<div>slide2</div>,
			<div>slide3</div>,
			<div>slide4</div>,
		]
	
		return (
			<Slider slides={slides} slidesToShow={3} speed={500} />
		)
	}
```
> For complete examples on how each prop works check the examples included (see [Development](#development))
#### Props
**slides** (type: *array*,  default: *[]*)
An array of slides to be shown. The items need to be html elements

**slidesToShow** (type: *int, float*,  default: *1*)
Number of slides to show

**slidesToScroll** (type: *int, float*,  default: *1*)
Number of slides to scroll

**infinite** (type: *boolean*,  default: *false*)
Toggles infinite loop sliding

**speed** (type: *int*,  default: *300*)
Slide animation speed in milliseconds

**autoplay** (type: *boolean*,  default: *false*)
Toggles the autoplay of slides

**autoplaySpeed** (type: *int*,  default: *2000*)
Autoplay speed in milliseconds'

**dots** (type: *boolean*,  default: *false*)
Toggles the visibility of the slides dots

**arrows** (type: *boolean*,  default: *true*)
Toggles the visibility of the slides arrows

**nativeMobile** (type: *boolean*,  default: *false*)
If true and the width of the page is < 1024) the slider uses native scroll

**free** (type: *boolean*,  default: *false*)
If true and the width of the page is < 1024) the slider scroll is free

**height** (type: *int*,  default: *undefined*)
If set, the slider will have the height set in pixels

**classPrefix** (type: *string*,  default: *empty string*)
The className prefix to be added to all the elements that have a className set
> Ex: classPrefix: "test-" will modify the main slider div to have the className "test-slider"

**classSuffix** (type: *string*,  default: *empty string*)
The className suffix to be added to all the elements that have a className set
> Ex: classSuffix: "-test" will modify the main slider div to have the className "slider-test"


## Development
Run examples

    git clone git@bitbucket.org:icoldoweb/awesome-react-carousel.git
    cd awesome-react-carousel
    yarn
    yarn start
    // a new tab will open in your default browser on http://localhost:8080
    
